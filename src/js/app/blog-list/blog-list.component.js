'use strict';

angular.module('blogList').
	component('blogList', {
		// template : "<div class=''><h1 class='first-class'>{{ title }}</h1><button ng-click='submitted()'>click</button></div>",
		templateUrl: '/templates/blog-list.html',
		controller : function($routeParams, $scope){
			console.log($routeParams)
			var blogItems = [
				{'id':1, 'name': 'Some item', 'description': 'some items'},
				{'id':2, 'name': 'Another item', 'description': 'another items'},
				{'id':3, 'name': 'Next item', 'description': 'next items'}
			]
			$scope.items = blogItems;
			$scope.title = 'Hai There';
			$scope.clicked = 0;
			$scope.submitted = function(){
				$scope.title = 'Clicked ' + $scope.clicked + ' times';
				$scope.clicked +=1;
			}

		}
	});




	// controller('BlogListController', function($scope){
	// 	$scope.title = 'Hai There';
	// 	$scope.clicked = 0;

	// 	$scope.submitted = function(){
	// 		$scope.title = 'Clicked ' + $scope.clicked + ' times';
	// 		$scope.clicked +=1;
	// 	}
	// 	console.log('hello');
		
	// });