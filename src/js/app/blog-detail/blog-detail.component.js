'use strict';

angular.module('blogDetail').
	component('blogDetail', {
		templateUrl: '/templates/blog-detail.html',
		controller : function($http, $location, $routeParams, $scope){
			console.log($routeParams)
			// var blogItems = [
			// 	{'id':1, 'name': 'Some item', 'description': 'some items'},
			// 	{'id':2, 'name': 'Another item', 'description': 'another items'},
			// 	{'id':3, 'name': 'Next item', 'description': 'next items'}
			// ]

			
			$http.get('/json/posts.json').then(callBackSuccess, errorCallBack);

			function callBackSuccess(response){
				$scope.notFound = true;
				console.log(response)
				var blogItems = response.data
				$scope.posts = response.data
				angular.forEach(blogItems, function(post){
					if (post.id == $routeParams.id){
						$scope.post = post;
						$scope.notFound = false;
					}
				})
			}


			function errorCallBack(response){
				$scope.notFound = true;
				console.log(response)

			}
			// angular.forEach(blogItems, function(post){
			// 	if (post.id == $routeParams.id){
			// 		$scope.post = post;
			// 		$scope.notFound = false;
			// 	}
			// })

			if ($scope.notFound) {
				$location.path("/")
				console.log("Not found")
			}

		}
	});

