'use strict';

angular.module('try').
	config(function($routeProvider, $locationProvider){

		$locationProvider.html5Mode({enabled: true})


		$routeProvider.
			when("/",{
				template: "<blog-list></blog-list>"
			}).
			when("/about", {
				templateUrl: "/templates/about.html"
			}).
			// when("/blog/1/", {
			// 	template: "<h1>Welcome to Some item</h1>"
			// }).
			// when("/blog/2/", {
			// 	template: "<h1>Welcome to Another item</h1>"
			// }).
			// when("/blog/3/", {
			// 	template: "<h1>Welcome to Next item</h1>"
			// }).

			when("/blog/:id/", {
				template: "<blog-detail></blog-detail>"
			}).
			when("/blog/:id/:abc", {
				template: "<blog-list></blog-list>"
			}).
			otherwise({
				template: "<h1>No data</h1>"
			})
	});